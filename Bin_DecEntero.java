
package complemento_2;


import java.util.Scanner;

/**
 * Binario a Decimal
 * @author Rendon Rios Teresita
 */
public class Bin_DecEntero {
    
    private String uno="1";
private static String tipo="";
    
    public void bin_dec(String scad) {
       
        int suma=0, o=scad.length()-1;
       for( int i = 0; i < scad.length(); i++) 
{
       suma = suma + Integer. parseInt("" + scad.charAt(o)) * (int) Math. pow(2, i) ;
         o--;
}
        System.out.println("   Decimal:   "+suma);

    }
    public  String sum_binari(String binario_1) 
    {
        if (binario_1 == null || uno == null) return "";
        int primero = binario_1.length() - 1;
        int segundo = uno.length() - 1;
        StringBuilder cadena = new StringBuilder();
        int acarreo = 0;
        
        while (primero >= 0 || segundo >= 0 ) 
        {
            int suma = acarreo;
            if (primero >= 0) 
            {
                suma += binario_1.charAt(primero) - '0';
                primero--;
            }
            if (segundo >= 0) 
            {
                suma += uno.charAt(segundo) - '0';
                segundo--;
            }
            acarreo = suma >> 1;
            suma = suma & 1;
            cadena.append(suma == 0 ? '0' : '1');
        }
        if (acarreo > 0)
        cadena.append('1');
        cadena.reverse();
        return String.valueOf(cadena);
    }

    public String inversor(String cadena)
 {
     StringBuilder inver = new StringBuilder(); 
inver = new StringBuilder(); 


for(int i=0;i<cadena.length();i++)
{
    if(cadena.charAt(i)=='0')
    {
        inver.append("1");
    }
    else
    
        if(cadena.charAt(i)=='1')
       { inver.append("0");
        }

 }

        return String.valueOf(inver);
        
 }
    public static String rellenar(int x)
{
    String d="";
    int tip;
              if(x>0 && x<9)
              {
                  tip=8;
                  tipo="Tipo: BYTE";
              }
            else  if(x>9&& x<17)
            {
                tip=16;
                tipo="Tipo: WORD";
            }
             else if(x>17 && x<33)
             {
                 tip=32;
                 tipo="Tipo: DWORD";
             }
            else  if(x>33 && x<65)
            {
                tip=64;
                tipo="Tipo: QWORD";
            }
            else{
                tip=0;
            }
              
              tip=tip-x;
              for(int i=0;i<tip;i++)
              {
                  d=d+"0";
              }
              return d;

}
    public  void signo(String cadena)
    {
        String  x= cadena.substring(0,1);
        
        if(x.equals("0")==true)
       {
           //positivo
           x=cadena.substring(1,cadena.length());
           System.out.println("  POSITIVO   (0)");
            System.out.println("  Binario:    "+rellenar(x.length())+x);
            
           bin_dec(x);
           System.out.println(tipo);
     
       }
       else
        //if(x.equals("1")==true)
       {
           //negativo
          x=cadena.substring(1,cadena.length());
           System.out.println("  NEGATIVO   (1)");
           System.out.println("  Binario:    "+rellenar(x.length())+x);
           negativo(x);        
       }
    }
 
    public static void main(String[] args) {
        
        
         Scanner leer = new Scanner(System.in);
        String numero_binario;
        System.out.print("   Ingresa:   ");
        numero_binario = leer.next();
       Bin_DecEntero com=new Bin_DecEntero();
       
     
       com.signo(numero_binario);

    }

    private void negativo(String g) {
       g=inversor(g);
       System.out.println("  Inverso:    "+rellenar(g.length())+g);
       g=sum_binari(g);
     System.out.println("Complemento:  "+rellenar(g.length())+g);
       bin_dec(g);
       System.out.println(tipo);
    
    }
}


package complemento_2;

import java.util.Scanner;
/**
 *Decimal a Binario
 * @author Rendon Rios Teresita
 */
public class EnteroDec_Bin {
private String uno="1";
private static String tipo="";
private int d;

public void setD(int dd)
{
    d=dd;
}

public void conver(int d, int f)
{String aux="";
    if(d==0)
    {
        aux=x(f);
        System.out.println("  Binario:  "+rellenar(aux.length())+aux);
        System.out.println(tipo);
    }
    else
    {
   f= Math.abs(f);
  
    aux=x(f);
    System.out.println("  Binario:      "+rellenar(aux.length())+aux);
    aux=rellenar(aux.length())+aux;
    System.out.println("  Inverso:      "+rellenar(aux.length())+inversor(aux));
    aux=inversor(aux);
    System.out.println("Complemento:    "+rellenar(aux.length())+sum_binari(aux));
    System.out.println(tipo);
    }
}
public static String rellenar(int x)
{
    String d="";
    int tip;
              if(x>0 && x<9)
              {
                  tip=8;
                  tipo="Tipo: BYTE";
              }
            else  if(x>9&& x<17)
            {
                tip=16;
                tipo="Tipo: WORD";
            }
             else if(x>17 && x<33)
             {
                 tip=32;
                 tipo="Tipo: DWORD";
             }
            else  if(x>33 && x<65)
            {
                tip=64;
                tipo="Tipo: QWORD";
            }
            else{
                tip=0;
            }
              
              tip=tip-x;
              for(int i=0;i<tip;i++)
              {
                  d=d+"0";
              }
              return d;
              
              
              

}

public int signo(int decimal)
{
    if(decimal>0)
        return 0;  //positivo
    else return 1; //negativo
        
}

public static void main(String[] args) {
        
        
         Scanner leer = new Scanner(System.in);
        int deci;
        System.out.print("   Ingresa:   ");
        deci = leer.nextInt();
       EnteroDec_Bin com=new EnteroDec_Bin();
       
       com.setD(com.signo(deci)); 

       
       com.conver(com.signo(deci),  deci);
    }

public String  x(int decimal){ 
 
String binario = ""; 
while ( decimal > 0 ) { 
binario = decimal % 2 + binario; 
decimal /= 2; 
} 
 return binario; 
} 


public String inversor(String cadena)
 {
     StringBuilder inver = new StringBuilder(); 
inver = new StringBuilder(); 


for(int i=0;i<cadena.length();i++)
{
    if(cadena.charAt(i)=='0')
    {
        inver.append("1");
    }
    else
    
        if(cadena.charAt(i)=='1')
       { inver.append("0");
        }

 }

        return String.valueOf(inver);
        
 }
    
public  String sum_binari(String binario_1) 
    {
        if (binario_1 == null || uno == null) return "";
        int primero = binario_1.length() - 1;
        int segundo = uno.length() - 1;
        StringBuilder cadena = new StringBuilder();
        int acarreo = 0;
        
        while (primero >= 0 || segundo >= 0 ) 
        {
            int suma = acarreo;
            if (primero >= 0) 
            {
                suma += binario_1.charAt(primero) - '0';
                primero--;
            }
            if (segundo >= 0) 
            {
                suma += uno.charAt(segundo) - '0';
                segundo--;
            }
            acarreo = suma >> 1;
            suma = suma & 1;
            cadena.append(suma == 0 ? '0' : '1');
        }
        if (acarreo > 0)
        cadena.append('1');
        cadena.reverse();
        return String.valueOf(cadena);
    }
}
